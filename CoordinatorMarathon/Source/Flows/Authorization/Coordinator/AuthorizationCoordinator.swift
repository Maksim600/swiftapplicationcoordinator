//
//  AuthorizationCoordinator.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class AuthorizationCoordinator: BaseCoordinator, AuthorizationCoordinatorOutput {
    
    var finishAuthorizationFlow: ((RouterProtocol) -> (Void))?
    
    private var router: RouterProtocol
    private var authorizationFactory: AuthorizationModuleFactoryProtocol
    
    init(router: RouterProtocol, authorizationFactory: AuthorizationModuleFactoryProtocol) {
        self.router = router
        self.authorizationFactory = authorizationFactory
    }
    
    // MARK: - Overriden
    
    override func start() {
        showRegistration()
    }
    
    // MARK: - Private
    
    private func showRegistration() {
        let registrationScreen = authorizationFactory.instantiateRegistrationScreen()
        registrationScreen.logInAction = { [unowned self] in
            self.showLogIn()
        }
        registrationScreen.successAuthorization = { [unowned self] in
            self.showLogIn()
        }
        
        router.setRootModule(registrationScreen, hideNavigationBar: true)
    }
    
    private func showLogIn() {
        let logInScreen = authorizationFactory.instantiateLogInScreen()
        logInScreen.createAction = { [unowned self] in
            self.router.popToRootModule()
        }
        logInScreen.successLogIn = { [unowned self] in
            self.finishAuthorizationFlow?(self.router)
        }
        logInScreen.forgotPasswordAction = { [unowned self] in
            self.router.setNavigationBarHidden(false)
            self.showForgotPassword()
        }
        logInScreen.viewWillAppear = { [unowned self] in
            self.router.setNavigationBarHidden(true)
        }
        
        router.push(logInScreen)
    }
    
    private func showForgotPassword() {
        let forgotPasswordScreen = authorizationFactory.instantiateForgotPasswordScreen()
        forgotPasswordScreen.createAction = { [unowned self] in
            self.router.setNavigationBarHidden(true)
            self.router.popToRootModule()
        }
        forgotPasswordScreen.logInAction = { [unowned self] in
            self.router.setNavigationBarHidden(true)
            self.router.popModule()
        }
        
        router.push(forgotPasswordScreen)
    }
    
}
