//
//  ProfileCoordinator.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/3/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class ProfileCoordinator: BaseCoordinator, ProfileCoordinatorOutput {

    var finishFlow: (() -> (Void))?
    
    private var router: RouterProtocol
    private var scenesFactory: ScenesFactoryProtocol
    private var coordinatorsFactory: CoordinatorsFactoryProtocol
    
    init(router: RouterProtocol, scenesFactory: ScenesFactoryProtocol, coordinatorsFactory: CoordinatorsFactoryProtocol) {
        self.router = router
        self.scenesFactory = scenesFactory
        self.coordinatorsFactory = coordinatorsFactory
    }
    
    // MARK: - Overriden
    
    override func start() {
        showProfileScreen()
    }
    
    // MARK: - Private
    
    private func showProfileScreen() {
        let profileScreen = scenesFactory.makeProfileModuleFactory().instantiateProfileScreen()
        profileScreen.subscriptionAction = { [unowned self] in
            self.runSubscriptionFlow()
        }
        profileScreen.finishFlow = { [unowned self] in
            self.finishFlow?()
        }
        
        router.setRootModule(profileScreen)
    }
    
    private func runSubscriptionFlow() {
        let subscriptionFactory = scenesFactory.makeSubscriptionModuleFactory()
        let subscriptionCoordinator = SubscriptionCoordinator(router: router, subscriptionFactory: subscriptionFactory)
        subscriptionCoordinator.start()
        addDependency(subscriptionCoordinator)
    }
    
}
