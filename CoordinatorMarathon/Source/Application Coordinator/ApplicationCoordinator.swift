//
//  ApplicationCoordinator.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class ApplicationCoordinator: BaseCoordinator {

    private let router: RouterProtocol
    private let applicationFactory: ApplicationFactory
    
    init(router: RouterProtocol, applicationFactory: ApplicationFactory) {
        self.applicationFactory = applicationFactory
        self.router = router
    }

    // MARK: - Overriden
    
    override func start() {
        showAuthorizationFlow()
    }
    
    // MARK: - Private
    
    private func showAuthorizationFlow() {
        let (authorizationCoordinator, module) = applicationFactory.coordinatorsFactory.makeAuthorizationCoordinator(router)
        authorizationCoordinator.finishAuthorizationFlow = { [unowned self] (router: RouterProtocol) in
            self.removeDependency(coordinator: authorizationCoordinator)
            self.showGeneralFlow()
        }
        
        addDependency(authorizationCoordinator)
        router.setRootModule(module, hideNavigationBar: true)
        authorizationCoordinator.start()
    }
    
    private func showGeneralFlow() {
        let (generalCoordinator, module) = applicationFactory.coordinatorsFactory.makeTabBarCoordinatorBox()
        generalCoordinator.finishFlow = { [unowned self] in
            self.removeDependency(coordinator: generalCoordinator)
            self.showAuthorizationFlow()
        }
        
        addDependency(generalCoordinator)
        router.setRootModule(module, hideNavigationBar: true)
        generalCoordinator.start()
    }

}
