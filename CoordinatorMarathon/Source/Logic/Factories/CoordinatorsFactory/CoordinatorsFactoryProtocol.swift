//
//  CoordinatorsFactoryProtocol.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

protocol CoordinatorsFactoryProtocol: class {
    func makeAuthorizationCoordinator(_ router: RouterProtocol) -> (Coordinator & AuthorizationCoordinatorOutput, toPresent: Presentable?)
    
    func makeTabBarCoordinatorBox() -> (Coordinator & TabBarCoordinatorOutput, toPresent: Presentable?)
    func makeHomeCoordinator(forNavigationController navigationController: UINavigationController?) -> Coordinator
    func makeProfileCoordinatorBox(forNavigationController navigationController: UINavigationController?) -> (Coordinator & ProfileCoordinatorOutput, toPresent: Presentable?)
    func makeSubsriptionCoordinator(_ router: RouterProtocol) -> Coordinator
    func makeMapCoordinatorBox(_ router: RouterProtocol) -> (Coordinator, toPresent: Presentable?)
}
