//
//  MapCoordinator.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/4/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class MapCoordinator: BaseCoordinator, MapCoordinatorOutput {

    var finishFlow: (() -> (Void))?
    
    private var router: RouterProtocol
    private var mapModuleFactory: MapModuleFactoryProtocol
    
    init(router: RouterProtocol, mapModuleFactory: MapModuleFactoryProtocol) {
        self.router = router
        self.mapModuleFactory = mapModuleFactory
    }
    
    // MARK: - Overriden
    
    override func start() {
        
    }
    
    // MARK: - Private
    
    private func showMapScreen() {
        let mapModule = mapModuleFactory.instantiateMapScreen()
        mapModule.stopAction = { [unowned self] in
            self.finishFlow?()
        }
        router.setRootModule(mapModule)
    }
    
}
