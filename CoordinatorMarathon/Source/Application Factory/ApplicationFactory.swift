//
//  ApplicationFactory.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import Foundation

class ApplicationFactory: NSObject {

    lazy var coordinatorsFactory: CoordinatorsFactoryProtocol = {
        return CoordinatorsFactory(scenesFactory: self.scenesFactory)
    }()
    
    lazy var scenesFactory: ScenesFactoryProtocol = {
        return ScenesFactory(servicesFactory: self.servicesFactory)
    }()
    
    lazy var servicesFactory: ServicesFactoryProtocol = {
        return ServicesFactory()
    }()
    
}
