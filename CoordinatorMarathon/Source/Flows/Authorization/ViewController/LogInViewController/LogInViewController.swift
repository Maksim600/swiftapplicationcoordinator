//
//  LogInViewController.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController, LogInScreen {

    var successLogIn: (() -> Void)?
    var forgotPasswordAction: (() -> Void)?
    var createAction: (() -> Void)?
    var viewWillAppear: (() -> Void)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewWillAppear?()
    }
    
    // MARK: - Actions
    
    @IBAction func loginButtonTapped(sender: AnyObject) {
        successLogIn?()
    }
    
    @IBAction func forgotPasswordButtonTapped(sender: AnyObject) {
        forgotPasswordAction?()
    }
    
    @IBAction func createButtonTapped(sender: AnyObject) {
        createAction?()
    }
}
