//
//  HomeCoordinator.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/4/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class HomeCoordinator: BaseCoordinator {

    private let router: RouterProtocol
    private let homeFactory: HomeModuleFactoryProtocol
    
    init(router: RouterProtocol, homeFactory: HomeModuleFactoryProtocol) {
        self.router = router
        self.homeFactory = homeFactory
    }
    
}
