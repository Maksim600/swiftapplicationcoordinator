//
//  AuthorizationModuleFactoryProtocol.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import Foundation

protocol AuthorizationModuleFactoryProtocol: class {
    func instantiateLogInScreen() -> LogInScreen
    func instantiateRegistrationScreen() -> RegistrationScreen
    func instantiateForgotPasswordScreen() -> ForgotPasswordScreen
}
