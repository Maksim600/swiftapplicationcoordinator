//
//  RegistrationViewController.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController, RegistrationScreen {

    var successAuthorization: (() -> Void)?
    var logInAction: (() -> Void)?
    
    // MARK: - Actions
    
    @IBAction func createButtonTapped(sender: AnyObject) {
        successAuthorization?()
    }
    
    @IBAction func logInButtonTapped(sender: AnyObject) {
        logInAction?()
    }
}
