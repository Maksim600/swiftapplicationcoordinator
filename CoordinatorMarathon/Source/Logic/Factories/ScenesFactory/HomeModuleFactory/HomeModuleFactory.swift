//
//  HomeModuleFactory.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/4/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class HomeModuleFactory: NSObject, HomeModuleFactoryProtocol {
    
    private var servicesFactory: ServicesFactoryProtocol
    
    init(servicesFactory: ServicesFactoryProtocol) {
        self.servicesFactory = servicesFactory
    }
    
    // MARK: - HomeModuleFactoryProtocol
    
    func instantiateHomeScreen() -> HomeScreen {
        let homeViewController = HomeViewController.controllerInStoryboard(homeStoryboard())
        
        return homeViewController
    }
    
    // MARK: - Private
    
    func homeStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "HomeStoryboard", bundle: nil)
    }
    
}
