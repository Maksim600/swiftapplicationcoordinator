//
//  ServicesFactoryProtocol.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

protocol ServicesFactoryProtocol: class {
    var locationManager: LocationManagerProtocol? { get set }
    var networkManager: NetworkManagerProtocol? { get set }
    var storageManager: StorageManagerProtocol? { get set }
}
