//
//  WelcomeScreen.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/3/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

protocol WelcomeScreen: BaseScreen {
    var subscriptionAction: (() -> (Void))? { get set }
    var closeAction: (() -> (Void))? { get set }
}
