//
//  SubscriptionCoordinator.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/3/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class SubscriptionCoordinator: BaseCoordinator {

    private var router: RouterProtocol
    private var subscriptionFactory: SubscriptionModuleFactoryProtocol
    
    init(router: RouterProtocol, subscriptionFactory: SubscriptionModuleFactoryProtocol) {
        self.router = router
        self.subscriptionFactory = subscriptionFactory
    }
    
    // MARK: - Overriden
    
    override func start() {
        showSubscriptionScreen()
    }
    
    // MARK: - Private
    
    private func showSubscriptionScreen() {
        let subscriptionScreen = subscriptionFactory.instantiateSubscriptionScreen()
        
        router.push(subscriptionScreen)
    }
}
