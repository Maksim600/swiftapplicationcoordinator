//
//  Router.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class Router: NSObject, RouterProtocol {

    private weak var rootViewController: UINavigationController?
    
    init(rootController: UINavigationController) {
        super.init()
        
        self.rootViewController = rootController
    }
    
    // MARK: - Presentable
    
    func toPresent() -> UIViewController? {
        return rootViewController
    }
    
    // MARK: - RouterProtocol
    
    func push(_ module: Presentable?) {
        push(module, withAnimation: true)
    }
    
    func push(_ module: Presentable?, withAnimation animated: Bool) {
        guard let viewController = module?.toPresent(), (viewController is UINavigationController == false) else { return }
        
        rootViewController?.pushViewController(viewController, animated: true)
    }
    
    func present(_ module: Presentable?) {
        present(module, withAnimation: true)
    }
    
    func present(_ module: Presentable?, withAnimation animated: Bool) {
        guard let viewController = module?.toPresent() else { return }
        
        rootViewController?.present(viewController, animated: animated, completion: nil)
    }
    
    func setRootModule(_ module: Presentable?) {
        setRootModule(module, hideNavigationBar: false)
    }
    
    func setRootModule(_ module: Presentable?, hideNavigationBar: Bool) {
        guard let viewController = module?.toPresent() else { return }
        
        rootViewController?.setViewControllers([viewController], animated: false)
        rootViewController?.isNavigationBarHidden = hideNavigationBar
    }
    
    func popModule() {
        popModule(withAnimation: true)
    }
    
    func popModule(withAnimation animated: Bool) {
        rootViewController?.popViewController(animated: animated)
    }
    
    func popToRootModule() {
        rootViewController?.popToRootViewController(animated: true)
    }
    
    func setNavigationBarHidden(_ isHidden: Bool) {
        rootViewController?.isNavigationBarHidden = isHidden
    }
    
    func dismissModule() {
        dismissModule(withAnimation: true)
    }
    
    func dismissModule(withAnimation animated: Bool) {
        rootViewController?.dismiss(animated: animated, completion: nil)
    }
}
