//
//  WelcomeScreenViewController.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/3/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class WelcomeScreenViewController: UIViewController, WelcomeScreen {

    var subscriptionAction: (() -> (Void))?
    var closeAction: (() -> (Void))?
    
    @IBAction func subscribeButtonTapped(_ sender: Any) {
        closeAction?()
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        subscriptionAction?()
    }
    
}
