//
//  ProfileModuleFactory.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/3/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class ProfileModuleFactory: NSObject, ProfileModuleFactoryProtocol {

    private var servicesFactory: ServicesFactoryProtocol
    
    init(servicesFactory: ServicesFactoryProtocol) {
        self.servicesFactory = servicesFactory
    }
    
    // MARK: - ProfileModuleFactoryProtocol
    
    func instantiateProfileScreen() -> ProfileScreen {
        let profileViewController = ProfileViewController.controllerInStoryboard(profileStoryboard())
        
        return profileViewController
    }
    
    // MARK: - Private
    
    func profileStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "ProfileStoryboard", bundle: nil)
    }
    
}
