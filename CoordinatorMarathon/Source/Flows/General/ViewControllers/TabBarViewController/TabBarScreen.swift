//
//  TabBarScreen.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

protocol TabBarScreen: BaseScreen {
//    var onViewDidLoad: ((UINavigationController) -> (Void))? { get set }
    var onViewDidLoad: ((UINavigationController) -> (Void))? { get set }
    
    var onHomeFlowSelect: ((UINavigationController) -> (Void))? { get set }
    var onWorkoutFlowSelect: ((UINavigationController) -> (Void))? { get set }
    var onMapFlowSelect: ((UINavigationController) -> (Void))? { get set }
    var onGroupsFlowSelect: ((UINavigationController) -> (Void))? { get set }
    var onProfileFlowSelect: ((UINavigationController) -> (Void))? { get set }
}
