//
//  ForgotPasswordViewController.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController, ForgotPasswordScreen {

    var logInAction: (() -> Void)?
    var createAction: (() -> Void)?

    // MARK: - Actions
    
    @IBAction func logInButtonTapped(sender: AnyObject) {
        logInAction?()
    }
    
    @IBAction func createButtonTapped(sender: AnyObject) {
        createAction?()
    }
}
