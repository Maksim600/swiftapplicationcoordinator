//
//  LogInScreen.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

protocol LogInScreen: BaseScreen {
    var successLogIn: (() -> Void)? { get set }
    var forgotPasswordAction: (() -> Void)? { get set }
    var createAction: (() -> Void)? { get set }
    var viewWillAppear: (() -> Void)? { get set }
}
