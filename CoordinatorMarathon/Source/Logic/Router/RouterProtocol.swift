//
//  RouterProtocol.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

protocol RouterProtocol: Presentable {

    func push(_ module: Presentable?)
    func push(_ module: Presentable?, withAnimation animated: Bool)
    
    func present(_ module: Presentable?)
    func present(_ module: Presentable?, withAnimation animated: Bool)
    
    func setRootModule(_ module: Presentable?)
    func setRootModule(_ module: Presentable?, hideNavigationBar: Bool)
    
    func popModule()
    func popModule(withAnimation animated: Bool)
    
    func popToRootModule()
    
    func setNavigationBarHidden(_ isHidden: Bool)
    
    func dismissModule()
    func dismissModule(withAnimation animated: Bool)
    
}
