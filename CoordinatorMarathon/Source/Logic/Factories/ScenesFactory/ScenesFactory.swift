//
//  ScenesFactory.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import Foundation

class ScenesFactory: NSObject, ScenesFactoryProtocol {

    private var servicesFactory: ServicesFactoryProtocol
    
    init(servicesFactory: ServicesFactoryProtocol) {
        self.servicesFactory = servicesFactory
    }
    
    // MARK: - ScenesFactoryProtocol
    
    func makeAuthorizationModuleFactory() -> AuthorizationModuleFactoryProtocol {
        return AuthorizationModuleFactory(servicesFactory: servicesFactory)
    }
    
    func makeTabBarModuleFactory() -> TabBarModuleFactoryProtocol {
        return TabBarModuleFactory(servicesFactory: servicesFactory)
    }
    
    func makeProfileModuleFactory() -> ProfileModuleFactoryProtocol {
        return ProfileModuleFactory(servicesFactory: servicesFactory)
    }
    
    func makeSubscriptionModuleFactory() -> SubscriptionModuleFactoryProtocol {
        return SubscriptionModuleFactory()
    }
    
    func makeHomeModuleFactory() -> HomeModuleFactoryProtocol {
        return HomeModuleFactory(servicesFactory: servicesFactory)
    }
    
    func makeGroupsModuleFactory() -> GroupsModuleFactoryProtocol {
        return GroupsModuleFactory()
    }
    
    func makeTrainingModuleFactory() -> TrainingModuleFactoryProtocol {
        return TrainingModuleFactory()
    }
    
}
