//
//  HomeModuleFactoryProtocol.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/4/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

protocol HomeModuleFactoryProtocol: class {
    func instantiateHomeScreen() -> HomeScreen
}
