//
//  AuthorizationCoordinatorOutput.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

protocol AuthorizationCoordinatorOutput: class {
    var finishAuthorizationFlow: ((RouterProtocol) -> (Void))? { get set }
}
