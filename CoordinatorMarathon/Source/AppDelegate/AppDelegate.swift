//
//  AppDelegate.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var rootController: UINavigationController {
        return self.window!.rootViewController as! UINavigationController
    }
    
    private lazy var applicationFactory: ApplicationFactory = {
        return ApplicationFactory()
    }()
    
    private lazy var applicationCoordinator: ApplicationCoordinator = {
        return ApplicationCoordinator(router: Router(rootController: rootController), applicationFactory: self.applicationFactory)
    }()
    
    // MARK: - UIApplicationDelegate
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        applicationCoordinator.start()
    
        return true
    }
 
}

