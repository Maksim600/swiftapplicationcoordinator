//
//  TabBarCoordinator.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class TabBarCoordinator: BaseCoordinator, TabBarCoordinatorOutput {
    
    private let shouldShowWelcomeScreen = true
    
    var finishFlow: (() -> (Void))?
    
    private var tabBarModuleFactory: TabBarModuleFactoryProtocol
    private var coordinatorsFactory: CoordinatorsFactoryProtocol
    
    private var tabBarScreen: TabBarScreen
    
    init(tabBarScreen: TabBarScreen, tabBarModuleFactory: TabBarModuleFactoryProtocol, coordinatorsFactory: CoordinatorsFactoryProtocol) {
        self.tabBarModuleFactory = tabBarModuleFactory
        self.coordinatorsFactory = coordinatorsFactory
        self.tabBarScreen = tabBarScreen
    }
    
    // MARK: - Overriden
    
    override func start() {
        tabBarScreen.onViewDidLoad = showProfileScreen()
    }
    
    // MARK: - Private

    private func viewDidLoadAction() -> ((UINavigationController) -> (Void)) {
        return showWelcomeScreen()
    }
    
    private func showWelcomeScreen() -> ((UINavigationController) -> (Void)) {
        return { navigationController in
            let router = Router(rootController: navigationController)
            let welcomeScreen = self.tabBarModuleFactory.instantiateWelcomeScreen()
            welcomeScreen.subscriptionAction = {
                router.dismissModule()
            }
            welcomeScreen.closeAction = {
                router.dismissModule()
            }
            router.present(welcomeScreen, withAnimation: false)
        }
    }
    
    private func showProfileScreen() -> ((UINavigationController) -> (Void)) {
        return { navigationController in
            let (coordinator, _) = self.coordinatorsFactory.makeProfileCoordinatorBox(forNavigationController: navigationController)
            coordinator.finishFlow = { [unowned self] in
                self.finishFlow?()
            }
            coordinator.start()
            self.addDependency(coordinator)
        }
    }
}
