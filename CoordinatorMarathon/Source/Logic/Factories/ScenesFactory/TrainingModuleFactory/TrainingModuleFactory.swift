//
//  TrainingModuleFactory.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/4/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class TrainingModuleFactory: NSObject, TrainingModuleFactoryProtocol {
    
    func instantiateMapScreen() -> MapScreen {
        let mapViewController = MapViewController.controllerInStoryboard(mapStoryboard())
        
        return mapViewController
    }
    
    // MARK: - Private

    private func mapStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "MapStoryboard", bundle: nil)
    }
    
    private func workoutStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "WorkoutStoryboard", bundle: nil)
    }
}
