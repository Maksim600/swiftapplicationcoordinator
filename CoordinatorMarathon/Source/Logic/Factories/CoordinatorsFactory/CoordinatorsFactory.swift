//
//  CoordinatorsFactory.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class CoordinatorsFactory: NSObject, CoordinatorsFactoryProtocol {

    private var scenesFactory: ScenesFactoryProtocol
    
    init(scenesFactory: ScenesFactoryProtocol) {
        self.scenesFactory = scenesFactory
    }
    
    // MARK: - CoordinatorsFactoryProtocol
    
    func makeAuthorizationCoordinator(_ router: RouterProtocol) -> (Coordinator & AuthorizationCoordinatorOutput, toPresent: Presentable?) {
        let authorizationFactory = scenesFactory.makeAuthorizationModuleFactory()
        let registrationModule = authorizationFactory.instantiateRegistrationScreen()
        let authorizationCoordinator = AuthorizationCoordinator(router: router, authorizationFactory: authorizationFactory)
        
        return (authorizationCoordinator, registrationModule)
    }
    
    func makeTabBarCoordinatorBox() -> (Coordinator & TabBarCoordinatorOutput, toPresent: Presentable?) {
        let tabBarFactory = scenesFactory.makeTabBarModuleFactory()
        let tabBarScreen = tabBarFactory.instantiateTabBarScreen()
        let tabBarCoordinator = TabBarCoordinator(tabBarScreen: tabBarScreen, tabBarModuleFactory: tabBarFactory, coordinatorsFactory: self)
        
        return (tabBarCoordinator, tabBarScreen)
    }
    
    func makeHomeCoordinator(forNavigationController navigationController: UINavigationController?) -> Coordinator {
        let homeModuleFactory = scenesFactory.makeHomeModuleFactory()
        let router = makeRouter(forNavigationController: navigationController)
        let homeCoordinator = HomeCoordinator(router: router, homeFactory: homeModuleFactory)
        
        return homeCoordinator
    }
    
    func makeProfileCoordinatorBox(forNavigationController navigationController: UINavigationController?) -> (Coordinator & ProfileCoordinatorOutput, toPresent: Presentable?) {
        let router = makeRouter(forNavigationController: navigationController)
        let profileCoordinator = ProfileCoordinator(router: router, scenesFactory: scenesFactory, coordinatorsFactory: self)
        
        return (profileCoordinator, router)
    }
    
    func makeSubsriptionCoordinator(_ router: RouterProtocol) -> Coordinator {
        let subscriptionFactory = scenesFactory.makeSubscriptionModuleFactory()
        let subscriptionCoordinator = SubscriptionCoordinator(router: router, subscriptionFactory: subscriptionFactory)
        
        return subscriptionCoordinator
    }
    
    func makeMapCoordinatorBox(_ router: RouterProtocol) -> (Coordinator, toPresent: Presentable?) {
        let mapModuleFactory = scenesFactory.makeTrainingModuleFactory()
        let mapCoordinator = MapCoordinator(router: router, mapModuleFactory: mapModuleFactory)
        let mapModule = mapModuleFactory.instantiateMapScreen()
        
        return (mapCoordinator, mapModule)
    }
    
    // MARK: - Private
    
    private func makeRouter(forNavigationController navController: UINavigationController?) -> Router {
        return Router(rootController: navigationController(navigationController: navController))
    }
    
    private func navigationController(navigationController: UINavigationController?) -> UINavigationController {
        if let navigationController = navigationController {
            return navigationController
        } else {
            return UINavigationController()
        }
    }
    
}
