//
//  TabBarModuleFactory.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/3/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class TabBarModuleFactory: NSObject, TabBarModuleFactoryProtocol {

    private var servicesFactory: ServicesFactoryProtocol
    
    init(servicesFactory: ServicesFactoryProtocol) {
        self.servicesFactory = servicesFactory
    }
    
    // MARK: - TabBarModuleFactoryProtocol
    
    func instantiateTabBarScreen() -> TabBarScreen {
        let tabBarViewController = TabBarViewController.controllerInStoryboard(generalStoryboard())
        
        return tabBarViewController
    }
    
    func instantiateWelcomeScreen() -> WelcomeScreen {
        let welcomeScreenViewController = WelcomeScreenViewController.controllerInStoryboard(generalStoryboard())
        
        return welcomeScreenViewController
    }
    
    // MARK: - Private
    
    private func generalStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "GeneralStoryboard", bundle: nil)
    }
    
}
