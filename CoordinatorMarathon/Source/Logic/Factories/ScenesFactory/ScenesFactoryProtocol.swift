//
//  ScenesFactoryProtocol.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import Foundation

protocol ScenesFactoryProtocol: class {
    func makeAuthorizationModuleFactory() -> AuthorizationModuleFactoryProtocol
    func makeTabBarModuleFactory() -> TabBarModuleFactoryProtocol
    func makeProfileModuleFactory() -> ProfileModuleFactoryProtocol
    func makeSubscriptionModuleFactory() -> SubscriptionModuleFactoryProtocol
    func makeHomeModuleFactory() -> HomeModuleFactoryProtocol
    func makeGroupsModuleFactory() -> GroupsModuleFactoryProtocol
    func makeTrainingModuleFactory() -> TrainingModuleFactoryProtocol
}
