//
//  SubscriptionModuleFactory.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/3/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class SubscriptionModuleFactory: NSObject, SubscriptionModuleFactoryProtocol {
    
    // MARK: - SubscriptionModuleFactoryProtocol
    
    func instantiateSubscriptionScreen() -> SubscriptionScreen {
        let subscriptionViewController = SubscriptionViewController.controllerInStoryboard(subscriptionStoryboard())
        
        return subscriptionViewController
    }
    
    // MARK: - Private
    
    private func subscriptionStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "SubscriptionStoryboard", bundle: nil)
    }
}
