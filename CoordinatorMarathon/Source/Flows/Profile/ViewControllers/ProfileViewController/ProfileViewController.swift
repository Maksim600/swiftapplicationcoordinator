//
//  ProfileViewController.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/3/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, ProfileScreen {

    var finishFlow: (() -> (Void))?
    var subscriptionAction: (() -> (Void))?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        title = "Profile"
    }
    
}

extension ProfileViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        if indexPath.row == 0 {
            cell.textLabel?.text = "Subscription options"
        } else {
            cell.textLabel?.text = "Logout"
        }
        
        
        return cell
    }
    
}

extension ProfileViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            subscriptionAction?()
        } else {
            finishFlow?()
        }
    }
}
