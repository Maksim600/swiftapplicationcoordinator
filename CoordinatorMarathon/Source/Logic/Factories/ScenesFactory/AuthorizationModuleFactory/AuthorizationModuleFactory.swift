//
//  AuthorizationModuleFactory.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class AuthorizationModuleFactory: NSObject, AuthorizationModuleFactoryProtocol {

    private var servicesFactory: ServicesFactoryProtocol
    
    init(servicesFactory: ServicesFactoryProtocol) {
        self.servicesFactory = servicesFactory
    }
    
    // MARK: - AuthorizationModuleFactoryProtocol
    
    func instantiateLogInScreen() -> LogInScreen {
        let logInViewController = LogInViewController.controllerInStoryboard(authorizationStoryboard())
        
        return logInViewController
    }
    
    func instantiateRegistrationScreen() -> RegistrationScreen {
        let registrationViewController = RegistrationViewController.controllerInStoryboard(authorizationStoryboard())
        
        return registrationViewController
    }
    
    func instantiateForgotPasswordScreen() -> ForgotPasswordScreen {
        let forgotPasswordViewController = ForgotPasswordViewController.controllerInStoryboard(authorizationStoryboard())
        
        return forgotPasswordViewController
    }
    
    // MARK: - Private
    
    private func authorizationStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "AuthorizationStoryboard", bundle: nil)
    }
    
}
