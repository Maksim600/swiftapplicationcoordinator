//
//  RegistrationScreen.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

protocol RegistrationScreen: BaseScreen {
    var successAuthorization: (() -> Void)? { get set }
    var logInAction: (() -> Void)? { get set }
}
