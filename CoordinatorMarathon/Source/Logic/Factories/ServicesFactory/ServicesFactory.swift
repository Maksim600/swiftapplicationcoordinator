//
//  ServicesFactory.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import Foundation

class ServicesFactory: NSObject, ServicesFactoryProtocol {

    lazy var locationManager: LocationManagerProtocol? = {
        return LocationManager()
    }()
    
    var networkManager: NetworkManagerProtocol? = {
        return NetworkManager()
    }()
    
    var storageManager: StorageManagerProtocol? = {
        return StorageManager()
    }()
    
}
