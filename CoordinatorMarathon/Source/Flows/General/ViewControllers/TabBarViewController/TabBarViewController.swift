//
//  TabBarController.swift
//  CoordinatorMarathon
//
//  Created by Maks Ovcharuk on 10/2/17.
//  Copyright © 2017 Ovcharuk Maksim. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController, TabBarScreen, UITabBarControllerDelegate {

    var onViewDidLoad: ((UINavigationController) -> (Void))?
    
    var onHomeFlowSelect: ((UINavigationController) -> (Void))?
    var onWorkoutFlowSelect: ((UINavigationController) -> (Void))?
    var onMapFlowSelect: ((UINavigationController) -> (Void))?
    var onGroupsFlowSelect: ((UINavigationController) -> (Void))?
    var onProfileFlowSelect: ((UINavigationController) -> (Void))?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
        if let controller = customizableViewControllers?.first as? UINavigationController {
            onViewDidLoad?(controller)
        }
    }
    
}
